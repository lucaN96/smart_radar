package classi;

import java.io.IOException;

import com.pi4j.io.gpio.*;



public class Led {
	private int pinNum;
	private GpioPinDigitalOutput pin;
	
	public Led(int pinNum){

		this.pinNum = pinNum;
		try {
		    GpioController gpio = GpioFactory.getInstance();
		    pin = gpio.provisionDigitalOutputPin(Config.pinMap[pinNum]);		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public synchronized void switchOn() throws IOException {
		pin.high();		
		// System.out.println("LIGHT ON - pin "+pin);
	}

	
	public synchronized void switchOff() throws IOException {
		pin.low();
		// System.out.println("LIGHT OFF - pin "+pin);		
	}
	public synchronized boolean isOn() throws IOException {
            return pin.isHigh();           
            // System.out.println("LIGHT ON - pin "+pin);
    }
	
}
