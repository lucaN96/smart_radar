package classi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;

import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener;

/** I used the code provided by our prof A. Ricci and modified some parts
 * 
 * modified by Neri Luca 0000758738
 *
 */
public class SerialMonitor implements SerialPortEventListener {
        SerialPort serialPort;
        
        /**
        * A BufferedReader which will be fed by a InputStreamReader 
        * converting the bytes into characters 
        * making the displayed results codepage independent
        */
       private BufferedReader input;
       
        /** The output stream to the port */
        private OutputStream output;
        /** Milliseconds to block while waiting for port open */
        private static final int TIME_OUT = 2000;
       
        int i;
        private String s="";
        public void start(String portName, int dataRate) {
                CommPortIdentifier portId = null;
                
                try {
                        portId = CommPortIdentifier.getPortIdentifier(portName);
                        // open serial port, and use class name for the appName.
                        serialPort = (SerialPort) portId.open(this.getClass().getName(),
                                        TIME_OUT);
                        
                        // set port parameters
                        serialPort.setSerialPortParams(dataRate,
                                        SerialPort.DATABITS_8,
                                        SerialPort.STOPBITS_1,
                                        SerialPort.PARITY_NONE);

                        // open the streams
                        input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
                       
                        output = serialPort.getOutputStream();//initialize the output stream with the port stream
                          
                        // add event listeners
                        serialPort.addEventListener(this);
                        serialPort.notifyOnDataAvailable(true);
                        
                        
                        
                       

                } catch (Exception e) {
                        System.err.println(e.toString());
                }
        }

        /**
         * This should be called when you stop using the port.
         * This will prevent port locking on platforms like Linux.
         */
        public synchronized void close() {
                if (serialPort != null) {
                        serialPort.removeEventListener();
                        serialPort.close();
                }
        }

        /**
         * Handle an event on the serial port. Read the data and print it.
         */
        @Override
        public synchronized void serialEvent(SerialPortEvent oEvent) {
                if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
                        try {
                                //String inputLine;
                                if(input.ready() && (s=input.readLine()) != null){
                                   //input detectioning
                                    
                                    //System.out.println(s);
                                    
                                    
                                }
                        } catch (Exception e) {
                                System.err.println(e.toString());
                        }
                }
                
        }
        /**
         * Method use to send data to the serial connection, method called by the GUI
         * @param message Message to send via serial
         */
        public void sendDatas(String message){

            try {
                
                output.write(message.getBytes(Charset.forName("UTF-8")));
                
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        /** 
         * Method used to send some info from the monitor to the control class (Centralina)
         * @return message to send back
         */
        public String returnDatas(){
            return s;
        }


}