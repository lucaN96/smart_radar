package classi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
/**
 * 
 * Class made by Neri Luca 0000758738
 *
 * Main class which "controls" the radar
 */
public class Centralina extends Thread {
    
    private static final float DIST_MIN = 0.5f;
    private static final float DETECTIONING_DIST = 0.8f;
    private Button buttonOn;
    private Button buttonOff;
    private Led ledOn;
    private Led ledDet;
    private Led ledTrack;
    private SerialMonitor sr; //serial monitor variable used to send/receive message to/from serial
    private String readedString="";
    private int trackingCounter=0;
    private boolean started = false;
    private boolean tracking = false;
    private int lastPrinted = 0;
    private int lastMin = 0;
    private boolean alreadyBlinked = false;
    private boolean alreadyPrinted = false;
    
    private File file = new File(System.getProperty("user.dir")+"/logFile.txt"); //log file
    //private CommPortIdentifier currPortId = null;
    
    /**
     * Class constructor
     */
    public Centralina(){
        buttonOn = new Button(4);
        buttonOff = new Button(5);
        ledOn = new Led(0);
        ledDet = new Led(2);
        ledTrack = new Led(3);
        sr = new SerialMonitor();
        if(file.exists()){
            file.delete();
        }
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
        //currPortId = CommPortIdentifier.getPortIdentifier("/dev/ttyACM0"); //selecting port
        
        //starting the serial monitor, with the given parameters (port name, baud rate)
        sr.start(currPortId.getName(), 9600); 
        
        
        
        this.run();
    }
    /**
     * Inherit method from the thread class
     */
    public void run(){
        System.out.println("System Ready, press the ON button to start");
        while(true){
            
            //checks if the on button is pressed
            if(buttonOn.isPressed() && !started){
                sr.sendDatas("O"); //must send the "on" command to the radar
                try {
                    System.out.println("Smart Radar started");
                    ledOn.switchOn();
                    
                    alreadyPrinted = false;
                    started = true; //boolean which states that the system  started
                } catch (IOException e) {
                    
                    e.printStackTrace();
                }
            }
            //off button is pressed
            else if(buttonOff.isPressed()){
                try {
                    ledOn.switchOff();
                    if(ledTrack.isOn()){
                        ledTrack.switchOff();
                    }
                    sr.sendDatas("S"); //stop message send
                    trackingCounter=0;
                    //sr.close();
                    if(!alreadyPrinted){
                        System.out.println("Radar stopped, press ON to restart");
                        alreadyPrinted=true;
                    }
                    
                    started = false;
                } catch (IOException e) {
                    
                    e.printStackTrace();
                }
            }if(started){//standard execution case
                
                readedString= sr.returnDatas();//reads the value received from the serial, thanks to serial monitor's method
                Calendar cal = Calendar.getInstance(); //used to print the current time
                if(readedString.length() > 0){
                    
                        String[] vett = readedString.split(" ");//splitting the input using space as delimiter 
                        //distance and rotation angle received
                        if(vett.length > 1){
                          
                            float f = Float.parseFloat(vett[1]); //parsing distance
                            
                            if(f< DETECTIONING_DIST){ //object detected, but too far for tracking
                                try {
                                    //detectioning led must blink
                                    if(!alreadyBlinked & !tracking){
                                        ledDet.switchOn();
                                        sleep(100);
                                        ledDet.switchOff();
                                        alreadyBlinked = true;
                                    }else{
                                        ledDet.switchOff();
                                        sleep(300);
                                    }
                                    if(f < DIST_MIN){ //object close enough to track
                                        
                                            sr.sendDatas("B"); //the servo must stop at that angle
                                            tracking=true;
                                            
                                      
                                        try {
                                            ledTrack.switchOn(); //tracking led on
                                            
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                        
                                        //called log method to print on the file and also on the console
                                        logger("Time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+"  Object tracked at angle "+vett[0] + " distance "+f);
                                    
                                    
                                    }else{ //object only detected, not tracked, simple message displayed on the console and stored in the file
                                       
                                        logger("Time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+" Object tracked at angle "+vett[0] );
                                    }
                                    
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                
                             }
                             if(f > DIST_MIN && tracking){ //object tracked is going away
                                sr.sendDatas("R"); // the servo can move again
                                trackingCounter++; //tracking counter must increase
                                try {
                                    ledTrack.switchOff();
                                    
                                    tracking = false;
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            
                            
                            }else if(f > DETECTIONING_DIST ){
                                alreadyBlinked=false;
                            }
                        
                        
                        //rotation cycle completed
                        } if(readedString.equals("completed")){
                             cal = Calendar.getInstance();
                            
                            int actualSec = cal.get(Calendar.SECOND);
                            int actualMin = cal.get(Calendar.MINUTE);
                            if((actualSec - lastPrinted)>=5 || (actualMin - lastMin)>=1){//condition used to prevent
                                lastPrinted = actualSec;
                                lastMin = actualMin;
                                //display and logging how many object were tracked during this rotation cycle
                                logger("Scanning cicle complete---Total object detected: "+trackingCounter);
                                readedString = "";
                               trackingCounter=0;
                            }
                          
                        } 
                
                }
                
                
            }
        }
    }//run method closing
    /**Method used to display and log on a file some messages
     * 
     * @param message info to display and save
     */
    public void logger(final String message) {
        try (BufferedWriter bw= new BufferedWriter(new FileWriter(file, true))) {
          bw.write(message+"\n"); //write on the file          
          System.out.println(message); //display in the console
        } catch (IOException exception) {
          System.out.println(exception.getMessage());
          exception.printStackTrace();
        }
      }

}
