//Made by Neri Luca
#ifndef __RADARTASK__
#define __RADARTASK__
#include "Task.h"
class RadarTask: public Task {



  public:
    RadarTask(int ledPin, int servoPin, int sonarPin, int trigPin);
    enum {MOVING,BLOCK,STOP, WAIT_INPUT} state,previousState; //enum which contains the task states   
    void init(int period);
    void tick();
    void getDistance();
  private:
    float currentDistance = 0;
    const float vs = 331.5 + 0.6 * 20;
    int ledPin;
    int servo;
    int sonarPin, trigPin;
    char data=' ';
    void readFromSerial();
};

#endif
