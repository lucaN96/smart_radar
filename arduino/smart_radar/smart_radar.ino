//Made by Neri Luca 

#include "Scheduler.h"
#include "SerialReaderTask.h"
#include "RadarTask.h"

//global variables
Scheduler sched;

float currentDistance = 0; //contains the info about the readed distance
String readedString = "";
void setup(){

  
  sched.init(20);
  Serial.begin(9600);
  
  //led, servo, echo, trig pins
  Task* t1 = new RadarTask(7,8,4,5);
  t1->init(200);
  sched.addTask(t1);

  Task* t2 = new SerialReaderTask();
  t2->init(20);
  sched.addTask(t2);

}

void loop(){
  
  sched.schedule();//scheduler in action
  
}
