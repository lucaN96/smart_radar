#ifndef __RUNNABLE__
#define __RUNNABLE__

class Runnable {
  
  virtual void schedule() = 0;
  
};

#endif

