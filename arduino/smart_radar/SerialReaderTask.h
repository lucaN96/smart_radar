//Made by Neri Luca
#ifndef __SERIALREADER__
#define __SERIALREADER__


#include "Task.h"

class SerialReaderTask: public Task {

    
  public:
    SerialReaderTask();
    
    void init(int period);
    void tick();

};

#endif
