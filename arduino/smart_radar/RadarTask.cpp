//Made by Neri Luca
#include "Arduino.h"
#include "RadarTask.h"
#include <Servo.h>
#define SERVO_INCREMENT 2
//shared variables

extern float currentDistance;
extern String readedString;
int pos = 90; //current servo position
int lastEndPos = 90; //stores the last end position reached by the servo (0 or 180)
bool block = false;
bool increasing = true;
Servo myServo;
//class constructor with hw pins
RadarTask::RadarTask(int ledPin, int servoPin, int sonarPin, int trigPin) {
  //hardware parameters initialization
  this->ledPin = ledPin;
  this->sonarPin = sonarPin;
  this->trigPin = trigPin;
  myServo.attach(servoPin);
  pinMode(ledPin, OUTPUT);
  pinMode(this->sonarPin, INPUT);
  pinMode(this->trigPin, OUTPUT);

}
//method used to get the distance
void RadarTask::getDistance() {
  digitalWrite(this->trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(this->trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(this->trigPin, LOW);

  float tUS = pulseIn(this->sonarPin, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;

  currentDistance = t * vs * 100;//converting the distance(meters) to centemeters
  
}
//method inherited from the task class, setups the period
void RadarTask::init(int period) {
  Task::init(period);
  state = WAIT_INPUT;
  previousState = WAIT_INPUT;//used to restore the previous state of the task after the block
  
}

//method that is called every "period" ms
void RadarTask::tick() {
  //do some operations based on the current task state
  switch (state) {

    case WAIT_INPUT: {
        //open message received
        if (readedString == "O") {
          
          digitalWrite(this->ledPin, HIGH);
          state = MOVING;//must switch state
          readedString = "";

          break;

        }
        readedString = "";
        break;
      }
    
    case MOVING: {


        getDistance(); //calculating distance
        readFromSerial(); //checks if a message is received
        
        if(state==STOP || block){ //if the stop message has been received...
          previousState = MOVING; //storing the current state
          readedString = "";
          break; //must exit from this state
        }
        //servo position must increase
        if (pos <= 180 && increasing) {
              pos += SERVO_INCREMENT;
              if (pos > 180 ) { //mas value reached
                if (lastEndPos == 0) { //controls if the cicle was complete, from 0 to 180
    
                  Serial.println("completed");
                }
                lastEndPos = 180;
                increasing = false;
              }else { //max value not reached yet
                myServo.write(pos); //servo positioning
                Serial.print(pos); Serial.print(" "); Serial.println(currentDistance / 100);
                
              }


        //the servo must decrease its position
        } else if (!increasing) {
          pos -= SERVO_INCREMENT;
          if (pos < 0) {
            if (lastEndPos == 180) {

              Serial.println("completed");
              increasing = true;
            }
            lastEndPos = 0;
            increasing = true;
          } else {
            myServo.write(pos);
            Serial.print(pos); Serial.print(" "); Serial.println(currentDistance / 100);
            
           
          }


        }//no block detected
        if (!block) {
          state = MOVING; //no need to switch state
          readedString = "";
          break;
        } /*else { //block message detected from the serial listener
          state = BLOCK;//MUST switch state
          previousState = MOVING; //storing the current state
          readedString = "";
          break;
        }*/
        readedString = "";
        
        //previousState = MOVING;
        break;

      }
      //the servo must block because an object is being tracked
    case BLOCK: {
        
        getDistance();
        readFromSerial(); //controls if new messages are available
        if(state==STOP){ //if the stop message has been received...
          break; //must exit from this state
        }
        //print info on the serial channel
        Serial.print(pos); Serial.print(" "); Serial.println(currentDistance / 100);
        
        
        readedString = "";
        break;
      }

    //system must stop its execution
    case STOP: {
        digitalWrite(this->ledPin, LOW);
        myServo.write(90); //setting the default position
        state = WAIT_INPUT;
        previousState = WAIT_INPUT;
        //turnOff = false;
        increasing = true;
        block = false;
        pos = 90;
        readedString = "";
        lastEndPos = 90;
        break;
      }


  }


}
//Method that checks if a message from the serial is received
void RadarTask::readFromSerial() {

  //block message received--> tracking phase
  if (readedString == "B") {
    readedString = "";
    block = true;
    state = BLOCK;
  }//release message received --> tracking phase ended
  else if (readedString == "R") {
    readedString = "";
    state = previousState;
    block = false;
  }//stop message received --> system must stop
  else if (readedString == "S") {
    readedString = "";
    block = false;
    state = STOP;
  }

}







