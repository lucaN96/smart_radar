//Made by Neri Luca
#include "SerialReaderTask.h"
#include "Arduino.h"
//shared variable that contains the readed value from the serial
extern String readedString;
//class constructor
SerialReaderTask::SerialReaderTask() {
  
 
}


void SerialReaderTask::init(int period) {
  Task::init(period);
  

}

void SerialReaderTask::tick() {
  
        //reads from the serial
        char data;
        if (Serial.available()) {
          data = Serial.read();
          readedString = data;
        }
        
}








